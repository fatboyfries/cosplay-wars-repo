﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TempCharacterSelectManager : MonoBehaviour
{
    public PlayerManager[] pMan;
    public CharacterSelectCursor[] cursorObj;
    public int currentPlayerSelect;
    public bool[] playerConnected;
    public string[] cursorControls = { "hori_contr_p", "vert_contr_p", "a_butt_p", "b_butt_p", "x_butt_p", "y_butt_p", "start_contr_p" };

    public bool levelTransition;
    public GameObject LoadingScreenCanvas;
    public GameObject MainMenuCanvas;
    public GameObject PlayerCanvas;

    void Update( )
    {
        if( !levelTransition )
        {
            CharacterSelectInit( );
            ToTheGame( );
        }
    }

    void CharacterSelectInit( )
    {
        for( int i=0; i < cursorObj.Length; i++ )
        {
            if( Input.GetButtonDown( cursorControls[6] + (i + 1) ) && !playerConnected[i] )
            {
                cursorObj[currentPlayerSelect].playerNumber = ( i + 1 );
                cursorObj[currentPlayerSelect].myMoves = new string[cursorControls.Length];
                for( int j=0; j < cursorControls.Length; ++j )
                {
                    cursorObj[currentPlayerSelect].myMoves[j] = cursorControls[j] + ( i + 1 );
                }
                cursorObj[currentPlayerSelect].gameObject.SetActive( true );
                playerConnected[i] = true;
                currentPlayerSelect++;
            }
        }
    }
    void ToTheGame( )
    {
        if( currentPlayerSelect == 1 )
        {
            if( cursorObj[0].hasGirl == true && !levelTransition )
            {
                //transition
                levelTransition = true;
                StartCoroutine( ToLevel( ) );
            }
        }
        else if( currentPlayerSelect == 2 )
        {
            if( cursorObj[0].hasGirl == true && cursorObj[1].hasGirl == true && !levelTransition )
            {
                //transition
                levelTransition = true;
                StartCoroutine( ToLevel( ) );
            }
        }
        else if( currentPlayerSelect == 3 )
        {
            if( cursorObj[0].hasGirl == true && cursorObj[1].hasGirl == true && cursorObj[2].hasGirl == true && !levelTransition )
            {
                //transition
                levelTransition = true;
                StartCoroutine( ToLevel( ) );
            }
        }
        else if( currentPlayerSelect == 4 )
        {
            if( cursorObj[0].hasGirl == true && cursorObj[1].hasGirl == true && cursorObj[2].hasGirl == true && cursorObj[3].hasGirl == true && !levelTransition )
            {
                //transition
                levelTransition = true;
                StartCoroutine( ToLevel( ) );
            }
        }
    }

    IEnumerator ToLevel( )
    {
        yield return new WaitForSeconds( 1.5f );
        MainMenuCanvas.SetActive( false );
        LoadingScreenCanvas.SetActive( true );
        yield return new WaitForSeconds( 2f );
        SceneManager.LoadSceneAsync( 1, LoadSceneMode.Additive );
        // stop menu music
        yield return new WaitForSeconds( 0.25f );
        // set player camera rect sizes
        if( currentPlayerSelect == 1 )
        {
            pMan[cursorObj[0].currentGirlNumber].playerCam.rect = new Rect( 0, 0, 1, 1 );
        }
        else if( currentPlayerSelect == 2 )
        {
            pMan[cursorObj[0].currentGirlNumber].playerCam.rect = new Rect( 0f, 0.5f, 1, 1 );
            pMan[cursorObj[1].currentGirlNumber].playerCam.rect = new Rect( 0f, -0.5f, 1, 1 );
        }
        else if( currentPlayerSelect == 3 )
        {
            pMan[cursorObj[0].currentGirlNumber].playerCam.rect = new Rect( -0.5f, 0.5f, 1, 1 );
            pMan[cursorObj[1].currentGirlNumber].playerCam.rect = new Rect( 0.5f, 0.5f, 1, 1 );
            pMan[cursorObj[2].currentGirlNumber].playerCam.rect = new Rect( 0, -0.5f, 1, 1 );
        }
        else if( currentPlayerSelect == 4 )
        {
            pMan[cursorObj[0].currentGirlNumber].playerCam.rect = new Rect( -0.5f, 0.5f, 1, 1 );
            pMan[cursorObj[1].currentGirlNumber].playerCam.rect = new Rect( 0.5f, 0.5f, 1, 1 );
            pMan[cursorObj[2].currentGirlNumber].playerCam.rect = new Rect( -0.5f, -0.5f, 1, 1 );
            pMan[cursorObj[3].currentGirlNumber].playerCam.rect = new Rect( 0.5f, -0.5f, 1, 1 );
        }
        // turn on player canvases
        PlayerCanvas.SetActive( true );
        for( int i=0; i < pMan.Length; i++ )
        {
            pMan[i].gameObject.SetActive( true );
        }
        for( int i=0; i < currentPlayerSelect; i++ )
        {
            pMan[cursorObj[i].currentGirlNumber].gameObject.SetActive( true );
            pMan[cursorObj[i].currentGirlNumber].myGirlPlayerNumber = cursorObj[i].playerNumber;
            pMan[cursorObj[i].currentGirlNumber].myGirlName = cursorObj[i].girlName;
            for( int j=0; j < cursorControls.Length; ++j )
            {
                pMan[cursorObj[i].currentGirlNumber].myPlayerMoves[j] = cursorObj[i].myMoves[j];
            }
            pMan[cursorObj[i].currentGirlNumber].Init( );
            // set their position from the level
            // get component of stage to set position
        }
        // play stage music
        LoadingScreenCanvas.SetActive( false );
    }
}
