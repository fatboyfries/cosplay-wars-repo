﻿using UnityEngine;
using UnityEngine.UI;

public class GirlManager : MonoBehaviour
{
    public string GirlName;
    public Sprite selectedSprite;
    public Image myImage;
    public bool isOccupied;
    
    public void SelectedGirl( )
    {
        isOccupied = true;
        myImage.sprite = selectedSprite;
    }
}
