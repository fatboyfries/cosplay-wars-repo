﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public string startLevel;

    public void Story() //Story button fuction.
    {
        SceneManager.LoadScene("Main Menu", LoadSceneMode.Single);
    }

    public void Multiplayer() //Multiplayer button fuction.
    {

    }

    public void Options() //Options button fuction.
    {

    }

    public void Quit() //Quit button fuction.
    {
        Debug.Log("Ouit!");
        Application.Quit();
    }
}
