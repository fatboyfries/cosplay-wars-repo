﻿using UnityEngine;

public class CharacterSelectCursor : MonoBehaviour
{
    public int playerNumber;
    public string girlName;
    public string[] myMoves = { "hori_contr_p", "vert_contr_p", "a_butt_p", "b_butt_p", "x_butt_p", "y_butt_p", "start_contr_p" };

    public Transform[] girlLocations; // the spots the cursor moves to when selecting a girl
    public int currentGirlNumber; // the number that cycles to get to the different girl spots when selecting a girl
    public bool hasGirl;

    void Start( )
    {        
        transform.position = girlLocations[0].position;
    }

    void Update( )
    {
        if( !hasGirl ) InputUpdate( );
    }

    int clamp;
    void InputUpdate( )
    {
        float h = Input.GetAxis( myMoves[0] );
        
        if( h > 0.25f && clamp == 0 ) // cycle and move to next girl going RIGHT
        {
            clamp = 5;
            currentGirlNumber = currentGirlNumber == girlLocations.Length - 1 ? 0 : currentGirlNumber + 1;
            transform.position = girlLocations[currentGirlNumber].position;
        }
        else if( h < -0.25f && clamp == 0 ) // cycle and move to previous girl going LEFT
        {
            clamp = 5;
            currentGirlNumber = currentGirlNumber <= 0 ? girlLocations.Length - 1 : currentGirlNumber - 1;
            transform.position = girlLocations[currentGirlNumber].position;
        }
        else if( h < 0.25f && h > -0.25f )
        {
            clamp = 0;
        }

        if( Input.GetButtonDown( myMoves[2] ) && !girlLocations[currentGirlNumber].GetComponentInParent<GirlManager>().isOccupied ) // check if girl isnt already selected
        {
            // a select girl
            Debug.Log( "Selecting Girl " + currentGirlNumber );
            girlLocations[currentGirlNumber].GetComponentInParent<GirlManager>().SelectedGirl( );
            girlName = girlLocations[currentGirlNumber].GetComponentInParent<GirlManager>().GirlName;
            hasGirl = true;
        }
    }
}
