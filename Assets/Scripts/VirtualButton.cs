﻿using UnityEngine;
using UnityEngine.EventSystems;

public class VirtualButton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    private bool pressedDown;

	public virtual void OnPointerDown( PointerEventData ped )
    {
        pressedDown = true;
    }
	public virtual void OnPointerUp( PointerEventData ped )
    {
        pressedDown = false;
    }

    public bool GetVituralButton( )
    {
        return pressedDown;
    }
}
