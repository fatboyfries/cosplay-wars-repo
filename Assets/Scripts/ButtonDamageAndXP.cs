﻿using UnityEngine;

public class ButtonDamageAndXP : MonoBehaviour
{
    public enum DebugTest
    {
        HEALTH,
        XP
    }
    [Tooltip( "Lets the code know which funtion to call from the Player Manager" )]
    public DebugTest test;

    [System.Serializable]
    public class NumberRangeTool
    {
        public int min;
        public int max;
    }
    [Tooltip( "Set the minimum and maximum numbers randomized for Health Dealt and XP Gained" )]
    public NumberRangeTool rangeTool;

    [System.Serializable]
    public class GetInAwake
    {   
        public VirtualButton vButt;
        public PlayerManager pMan;
    }
    [Tooltip( "The Components the Script Gets on Awake by Find() or GetComponent()" )]
    public GetInAwake getInAwake;

    void Awake( )
    {
        getInAwake.vButt = GetComponent<VirtualButton>( );
        getInAwake.pMan = FindObjectOfType<PlayerManager>( );
    }

    void Update( )
    {
        switch( test )
        {
            case DebugTest.HEALTH:
                if( getInAwake.vButt.GetVituralButton( ) )
                {
                    getInAwake.pMan.UpdatePlayerHealth( Random.Range( rangeTool.min, rangeTool.max ) );
                }
                break;
            case DebugTest.XP:
                if( getInAwake.vButt.GetVituralButton( ) )
                {
                    getInAwake.pMan.UpdatePlayerXP( Random.Range( rangeTool.min, rangeTool.max ) );
                }
                break;
        }
    }
}
