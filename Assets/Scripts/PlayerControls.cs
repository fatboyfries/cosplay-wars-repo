﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;

public class PlayerControls : MonoBehaviour
{
    // just controls the characters movements, attacks, and interactions with shop and items
    public string myName = "Player 105"; // this gets set when player select their girl
    public float moveSpeed = 10.0f;
    public float jumpForce = 10.0f;
    public float gravity = 14.0f;
    public bool isTransitioning = false;
    public bool isOnLadder = false;
    private float verticalVelocity = 0.0f;
    private bool isHit = false;
    private bool isFacingRight = true;
    public bool isAttacking = false;
    public bool isFiring = false;

    public RuntimeAnimatorController[] anims; // 0 - idle, 1 - pose, 2 - walk, 3 - neutral jump, 4 - forward jump
    public Transform sprite;
    public SciptableAttacks[] myAttacks;
    [System.Serializable]
    public class FireTools
    {
        public float animTime;
        public RuntimeAnimatorController fireAnim;
        public float fireTime;
        public float projectileSpeed;
        public int projectileDamage;
        public GameObject projectile;
    } public FireTools fireTools;

    [System.Serializable]
    public class LadderTools
    {
        public float transitionTime = 0.25f; //time it takes to transition/lerp
        public GameObject transitionLocation; //where you're transitioning to

        [HideInInspector]public float _transitionTime;
        [HideInInspector]public bool isTransition = false;
        [HideInInspector]public bool isStartUp = false;
        [HideInInspector]public bool isEnding = false;
        [HideInInspector]public Vector3 pVector = Vector3.zero;
    } public LadderTools ladderTools;

    [System.Serializable]
    public class GetInAwake
    {        
        public Animator animator;
        public CharacterController controller;
        public CamFollowPanel myCam;
        // change virtual joy and but to keyboard and/or controller
        public string[] InputControls = { "hori_contr_p", "vert_contr_p", "a_butt_p", "b_butt_p", "x_butt_p", "y_butt_p", "start_contr_p" };
        // player manager gives the player their controls number
    }
    [Tooltip( "The Components the Script Gets on Awake by Find() or GetComponent()" )]
    public GetInAwake getInAwake;

    void Awake( )
    {
        getInAwake.animator = sprite.GetComponent<Animator>( );
        getInAwake.controller = GetComponent<CharacterController>( );
        getInAwake.myCam = GetComponentInChildren<CamFollowPanel>( );
        getInAwake.myCam.player = gameObject;

        isFacingRight = true;
        sprite.transform.localRotation = Quaternion.Euler( 0, 0, 0 );
        ladderTools._transitionTime = ladderTools.transitionTime;
    }

    void Update( )
    {
        if( !isHit )
        {
            if( !isTransitioning && !isAttacking && !isFiring )
                MovementUpdate( );
            if( isAttacking )
            {
                AttackUpdate( );
            }
            if( isFiring )
            {
                FireUpdate( );
            }
            if( isTransitioning )
            {
                LadderTransitionUpdate( );
            }
        }
        else
        {
            HitUpdate( );
        }
    }

    void MovementUpdate( )
    {
        float h = Input.GetAxis( getInAwake.InputControls[0] ) * Time.deltaTime * moveSpeed;
        float z = Input.GetAxis( getInAwake.InputControls[1] ) * Time.deltaTime * moveSpeed;

        if( getInAwake.controller.isGrounded )
        {
            if( h <= -0.015f ) // left
            {
                isFacingRight = false;
                getInAwake.animator.runtimeAnimatorController = anims[2];
                sprite.transform.localRotation = Quaternion.Euler( 0, 180, 0 );
            }
            else if( h >= 0.015f ) // right
            {
                isFacingRight = true;
                getInAwake.animator.runtimeAnimatorController = anims[2];
                sprite.transform.localRotation = Quaternion.Euler( 0, 0, 0 );
            }
            else if( z != 0 ) // up
            {
                getInAwake.animator.runtimeAnimatorController = anims[2];
                sprite.transform.localRotation = Quaternion.Euler( 0, isFacingRight ? 0 : 180, 0 );
            }
            else
            {
                if( getInAwake.animator.runtimeAnimatorController != anims[1] ) //getInAwake.animator.runtimeAnimatorController != anims[0] && 
                {
                    int rand = Random.Range( 0, 2 );
                    getInAwake.animator.runtimeAnimatorController = anims[1];
                }
            }

            verticalVelocity = -gravity * Time.deltaTime;
            if( Input.GetButtonDown( getInAwake.InputControls[2] ) && !isOnLadder && !isTransitioning )
            {
                verticalVelocity = jumpForce;
            }
            if( Input.GetButtonDown( getInAwake.InputControls[4] ) )
            {
                isAttacking = true;
                StartCoroutine( StartAttackSequence( ) );
                return;
            }
            if( Input.GetButtonDown( getInAwake.InputControls[3] ) )
            {
                isFiring = true;
                StartCoroutine( StartFireSequence( ) );
                return;
            }
        }
        else
        {
            /*if( h == 0 && z == 0 )
                getInAwake.animator.runtimeAnimatorController = anims[3];
            else*/
            getInAwake.animator.runtimeAnimatorController = anims[4];
            verticalVelocity -= gravity * Time.deltaTime;
        }

        Vector3 moveVector = new Vector3( h, verticalVelocity, z );
        getInAwake.controller.Move( moveVector * Time.deltaTime );
    }

    void LadderTransitionUpdate( )
    {
        if( ladderTools.isStartUp )
        {
            transform.position = Vector3.Lerp( transform.position, new Vector3( transform.position.x, ladderTools.pVector.y + 2, transform.position.z ), ladderTools.transitionTime );
            if( transform.position.y >= 1.9f )
            {
                StartCoroutine( Transition() );
            }
        }
        if( ladderTools.isTransition )
        {
            transform.position = Vector3.Lerp( transform.position, ladderTools.pVector, ladderTools.transitionTime );
            if( Vector3.Distance( transform.position, ladderTools.pVector ) <= 0.1f && Vector3.Distance( transform.position, ladderTools.pVector ) >= -0.1f )
            {
                ladderTools.isTransition = false;
                StartCoroutine( EndTransitionToLocation() );
            }
        }
        if( ladderTools.isEnding )
        {
            transform.position = Vector3.Lerp( transform.position, new Vector3( transform.position.x, ladderTools.pVector.y + 2, transform.position.z ), ladderTools.transitionTime );
            if( transform.position.y <= ladderTools.pVector.y + 0.7f )
            {
                ladderTools.isEnding = false;
                StartCoroutine( EndTransitionToLocation() );
            }
        }
    }
    void OnTriggerEnter( Collider other )
    {
        if( other.tag == "CamTrigger" )
        {
            if( !getInAwake.myCam.isRailingOnPanel )
            {
                CamTrigger cMan = other.GetComponent<CamTrigger>();
                getInAwake.myCam.transform.SetParent( cMan.transform );
                getInAwake.myCam.camPlace = cMan.cameraPlace;
                getInAwake.myCam.SetPosition( );
                getInAwake.myCam.isRailingOnPanel = !cMan.lockCamPosition;
            }
        }
    }
    void OnTriggerStay( Collider other )
    {
        if( other.tag == "Ladder" )
        {
            isOnLadder = true;
            if( Input.GetButtonDown( getInAwake.InputControls[2] ) && !isTransitioning && !isAttacking )
            {
                getInAwake.animator.runtimeAnimatorController = anims[3];
                isTransitioning = true;
                getInAwake.myCam.isRailingOnPanel = false;
                getInAwake.myCam.transform.SetParent( transform );
                LadderTransition lMan = other.GetComponent<LadderTransition>( );
                ladderTools.transitionLocation = lMan.transitionLocation;
                ladderTools.pVector = new Vector3( lMan.transitionLocation.transform.position.x, lMan.transitionLocation.transform.position.y + 0.65f, lMan.transitionLocation.transform.position.z );
                ladderTools.isTransition = true;
                ladderTools.isEnding = false;
                ladderTools.isStartUp = false;
            }
        }
    }
    void OnTriggerExit( Collider other )
    {
        if( other.tag == "Ladder" )
        {
            isOnLadder = false;
        }
    }
    IEnumerator Transition( )
    {
        ladderTools.isStartUp = false;
        yield return new WaitForSeconds( 0.5f );
        ladderTools.isTransition = true;
    }
    IEnumerator EndTransitionToLocation( )
    {
        yield return new WaitForSeconds( 0.15f );
        isTransitioning = false;
        getInAwake.animator.runtimeAnimatorController = anims[0];
    }

    bool startUpdate = false;
    int currentAnim = 0;
    public List<float> floatList;
    public List<RuntimeAnimatorController> animList;
    int currentAttack = 0;
    float timer;
    float currentAttackTimer;
    void AttackUpdate( )
    {
        if( !startUpdate )
            return;

        floatList[currentAnim] -= Time.deltaTime;
        if( floatList[currentAnim] <= 0 )
        {
            currentAnim++;
            if( currentAnim >= floatList.Count )
            {
                currentAnim = 0;
                isAttacking = false;
                startUpdate = false;
                floatList.Clear( );
                animList.Clear( );
                return;
            }
            getInAwake.animator.runtimeAnimatorController = animList[currentAnim];
        }

        if( floatList[currentAnim] >= timer )
        {
            if( Input.GetButtonDown( getInAwake.InputControls[4] ) ) // 4 = X
            {
                currentAttack++;
                if( currentAttack <= myAttacks.Length - 1 )
                {
                    //Debug.Log( currentAttack );
                    //Debug.Log( myAttacks[currentAttack].AttackName );
                    floatList.Add( myAttacks[currentAttack].FrameWindow );
                    animList.Add( myAttacks[currentAttack].AttackAnim );
                    timer = myAttacks[currentAttack].ChainTimer;
                }
            }
        }
    }

    IEnumerator StartAttackSequence( )
    {
        currentAttack = 0;
        timer = myAttacks[currentAttack].ChainTimer;
        currentAttackTimer = myAttacks[currentAttack].FrameWindow;
        getInAwake.animator.runtimeAnimatorController = myAttacks[currentAttack].AttackAnim;
        floatList.Add( currentAttackTimer );
        animList.Add( myAttacks[currentAttack].AttackAnim );
        yield return new WaitForSeconds( 0.01f );
        startUpdate = true;
    }
    public int ReturnAttackDamage( )
    {
        return myAttacks[currentAnim].AttackDamage;
    }
    public bool ReturnAttackPushBack( )
    {
        return myAttacks[currentAnim].PushBack;
    }

    float fireAnimTimer;
    float fireTimer;
    bool fireUpdate = false;
    bool hasFired = false;
    void FireUpdate( )
    {
        if( !fireUpdate )
            return;

        fireAnimTimer -= Time.deltaTime;
        if( fireAnimTimer <= 0 )
        {
            isFiring = false;
            fireUpdate = false;
            hasFired = false;
        }
        if( !hasFired ) fireTimer -= Time.deltaTime;
        if( fireTimer <= 0 )
        {
            hasFired = true;
            fireTimer = 5;
            Quaternion rot = Quaternion.Euler( 0, isFacingRight ? 0 : 180, 0 );
            GameObject go = (GameObject)Instantiate( fireTools.projectile, transform.position, rot );
            go.GetComponent<Rigidbody>().velocity = new Vector3( ( isFacingRight ? fireTools.projectileSpeed * 1 : fireTools.projectileSpeed * -1 ), 0, 0 );
            Destroy( go, 5f );
        }
    }
    IEnumerator StartFireSequence( )
    {
        fireAnimTimer = fireTools.animTime;
        fireTimer = fireTools.fireTime;
        getInAwake.animator.runtimeAnimatorController = fireTools.fireAnim;
        yield return new WaitForSeconds( 0.01f );
        fireUpdate = true;
    }

    public void JumpAttack( )
    {
        getInAwake.animator.runtimeAnimatorController = anims[3];
        verticalVelocity = jumpForce;
        /*RaycastHit hit;
        Debug.DrawRay( new Vector3(transform.position.x, transform.position.y - 0.25f, 0), Vector3.down, Color.red, 0.25f );
        if( Physics.Raycast( new Vector3(transform.position.x, transform.position.y - 0.25f, 0), Vector3.down, out hit, 0.25f) )
        {
            if( hit.transform.tag == "EnemySnail" )
            {
                hit.transform.GetComponent<EnemySnail>().Hit( );
                animator.runtimeAnimatorController = anims[2];
                verticalVelocity = jumpForce;
            }
        }*/
    }

    public void HitBySnail( )
    {
        isHit = true;
        getInAwake.animator.runtimeAnimatorController = anims[3];
        verticalVelocity = jumpForce / 100;
        StartCoroutine( Hit() );
    }

    void HitUpdate( )
    {
        Vector3 moveVector = new Vector3( (isFacingRight) ? -moveSpeed / 50 : moveSpeed / 50, verticalVelocity, 0 );
        getInAwake.controller.Move( moveVector * Time.deltaTime );

        verticalVelocity -= (gravity / 4) * Time.deltaTime;
    }

    IEnumerator Hit( )
    {
        yield return new WaitForSeconds( 2.0f );
        isHit = false;
    }
}
