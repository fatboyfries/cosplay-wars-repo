﻿using UnityEngine;

public class NextLevelLoader : MonoBehaviour
{
    private LevelGenerator levelGen;

    void Awake( )
    {
        levelGen = FindObjectOfType<LevelGenerator>( );
    }

    void OnTriggerEnter( Collider other )
    {
        if( other.tag == "Player" )
        {
            levelGen.NextStage( );
        }
    }
}
