﻿using UnityEngine;

public class PlayerJumpAttackCheck : MonoBehaviour
{
    public EnemySnail snail;
    private BoxCollider boxCollider;

    void Start( )
    {
        boxCollider = GetComponent<BoxCollider>( );
        boxCollider.isTrigger = true;
    }

    void OnTriggerEnter( Collider other )
    {
        if( other.tag == "Player" )
        {
            snail.Hit( );
            Destroy( boxCollider );
            other.GetComponent<PlayerControls>().JumpAttack( );
        }
    }
}
