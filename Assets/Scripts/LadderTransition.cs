﻿using System.Collections;
using UnityEngine;

public class LadderTransition : MonoBehaviour
{
    public bool isOpen = true; //some sort of field to check if ladder is accessible
    public GameObject transitionLocation; //where you're transitioning to
}
