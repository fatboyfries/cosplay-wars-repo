﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Attack", menuName = "Character Attack", order = 3)]
public class SciptableAttacks : ScriptableObject
{
    public string AttackName;
    public int AttackDamage;
    public float FrameWindow;
    public float ChainTimer;
    public bool PushBack = false;
    public RuntimeAnimatorController AttackAnim;
}
