﻿using UnityEngine;

[CreateAssetMenu( fileName = "Level 1", menuName = "Leveling System", order = 0 )]
public class ScriptableLevelingSystem : ScriptableObject
{
    //each class keeps track of
    public int maxHealth; // - max health                               = int
    public int maxXP; // - max xp needed to reach next level            = int
    public int coinsGained; // - coins gained( maybe? )                 = int
    public int attackDamage; // - attack level                          = int
    public bool receiveCrystalSlot; // - slot for extra crystal         = bool
    public int replenishCrystals; // - if we give them a mana crystal   = int( how many crystals we give them or should it just be a replenish )
}
