﻿using UnityEngine;

[CreateAssetMenu( fileName = "Item 1", menuName = "Buyable Items", order = 1 )]
public class ScriptableItems : ScriptableObject
{
    // these are the purchase-able items and ones we find in boxes
    // - item name                              = string
    // - item picture                           = Sprite
    // - item health regain                     = int
    // - item attack timer( super mario star )  = float
}
