﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySnail : MonoBehaviour
{
    public int amount = 200;
    public GameObject sprite;
    public RuntimeAnimatorController[] anims;

    private BoxCollider boxCollider;
    private Animator animator;

    void Start( )
    {
        animator = sprite.GetComponent<Animator>( );
        animator.runtimeAnimatorController = anims[0];
        boxCollider = GetComponent<BoxCollider>( );
        boxCollider.isTrigger = true;
    }

    public void Hit( )
    {
        animator.runtimeAnimatorController = anims[1];
        Destroy( boxCollider );
        Destroy( gameObject, 1.0f );
    }

    void OnTriggerEnter( Collider other )
    {
        if( other.tag == "Player" )
        {
            other.GetComponent<PlayerControls>().HitBySnail( );
        }
    }
}
