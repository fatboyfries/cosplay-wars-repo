﻿using UnityEngine;
using System.Collections.Generic;

public class LevelGenerator : MonoBehaviour
{
    public Texture2D map;
    public Texture2D[] maps;
    public ColorToPrefab[] colorMappings;

    private int currentMap;
    private List<GameObject> worldObjects = new List<GameObject>( );
    private Vector3 playerRespawnPoint;

    void Start( )
    {
        currentMap = 0;
        map = maps[currentMap];
        GenerateLevel( );
    }

    void GenerateLevel( )
    {
        ClearLevel( );

        for( int x=0; x < map.width; x++ )
        {
            for( int y=0; y < map.height; y++ )
            {
                GenerateTile( x, y );
            }
        }
    }
    public void NextStage( )
    {
        currentMap++;
        if( currentMap > maps.Length - 1 )
            currentMap = 0;
        map = maps[currentMap];
        GenerateLevel( );
    }

    void ClearLevel( )
    {
        for( int i=0; i < worldObjects.Count; i++ )
        {
            Destroy( worldObjects[i] );
        }
    }

    void GenerateTile( int x, int y )
    {
        Color pixelColor = map.GetPixel( x, y );

        if( pixelColor.a != 0 )
        {
            foreach( ColorToPrefab colorMapping in colorMappings )
            {
                if( colorMapping.color.Equals( pixelColor ) )
                {
                    Vector3 position = new Vector3( x, 0, y );
                    GameObject go = Instantiate(colorMapping.prefab);
                    go.name = colorMapping.name;
                    go.transform.position = position;
                    go.transform.SetParent( colorMapping.parent );
                    worldObjects.Add( go );
                    if( colorMapping.name == "Player" )
                        playerRespawnPoint = position;
                }
            }
        }
    }
}
