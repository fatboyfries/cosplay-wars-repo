﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCBaseManager : MonoBehaviour
{
    public enum NPCType
    {
        GENERIC, MOMOKUN, VAMPY_BIT_ME, YAYAHAN, NIGRI
    }
    public NPCType npcType;

    [System.Serializable]
    public class HiddenKeyTool
    {
        public GameObject Key;
        public SpeechBubbleTool sbTool;
    }
    public HiddenKeyTool hkTool;

    [System.Serializable]
    public class SpeechBubbleTool
    {
        [TextArea]
        public string Quote;
        public GameObject SpeechBubbleParent;
        public Text QuoteText;

        public float speechSpeed = 0.5f;
        public float durationTime = 2.0f;
        public bool isTyping = false;
    }
    public SpeechBubbleTool sbTool; /// generic quote

    void Update( )
    {
        if( Input.GetKeyDown(KeyCode.Space) )
        {
            Interact( "" );
        }
    }

    public GameObject Interact( string name )
    {
        if( name == "MOMOKUN" && npcType == NPCType.MOMOKUN )
        {
            if( !hkTool.sbTool.isTyping ) StartCoroutine( HiddenKeySpeech( ) );
            return hkTool.Key;
        }
        else if( name == "VAMPY_BIT_ME" && npcType == NPCType.VAMPY_BIT_ME )
        {
            if( !hkTool.sbTool.isTyping ) StartCoroutine( HiddenKeySpeech( ) );
            return hkTool.Key;
        }
        else if( name == "YAYAHAN" && npcType == NPCType.YAYAHAN )
        {
            if( !hkTool.sbTool.isTyping ) StartCoroutine( HiddenKeySpeech( ) );
            return hkTool.Key;
        }
        else if( name == "NIGRI" && npcType == NPCType.NIGRI )
        {
            if( !hkTool.sbTool.isTyping ) StartCoroutine( HiddenKeySpeech( ) );
            return hkTool.Key;
        }
        else
        {
            if( !sbTool.isTyping ) StartCoroutine( GenericSpeech( ) );
            return null;
        }
    }

    IEnumerator HiddenKeySpeech( )
    {
        hkTool.sbTool.isTyping = true;
        int letter = 0;
        hkTool.sbTool.QuoteText.text = "";
        hkTool.sbTool.SpeechBubbleParent.SetActive( true );

        while( letter < hkTool.sbTool.Quote.Length - 1 )
        {
            hkTool.sbTool.QuoteText.text += hkTool.sbTool.Quote[letter];
            letter += 1;
            yield return new WaitForSeconds( hkTool.sbTool.speechSpeed );
        }

        hkTool.sbTool.QuoteText.text = hkTool.sbTool.Quote;

        /// play speech sound too...
        yield return new WaitForSeconds(hkTool.sbTool.durationTime );

        hkTool.sbTool.SpeechBubbleParent.SetActive( false );
        hkTool.sbTool.isTyping = false;
    }

    IEnumerator GenericSpeech( )
    {
        sbTool.isTyping = true;
        int letter = 0;
        sbTool.QuoteText.text = "";
        sbTool.SpeechBubbleParent.SetActive( true );

        while( letter < sbTool.Quote.Length - 1 )
        {
            sbTool.QuoteText.text += sbTool.Quote[letter];
            letter += 1;
            yield return new WaitForSeconds( sbTool.speechSpeed );
        }

        sbTool.QuoteText.text = sbTool.Quote;

        /// play speech sound too...
        yield return new WaitForSeconds( sbTool.durationTime );

        sbTool.SpeechBubbleParent.SetActive( false );
        sbTool.isTyping = false;
    }
}
