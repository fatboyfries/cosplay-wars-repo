﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    // make a singleton and dont destroy on load


    // player manager script that keeps track of some things...
    // i'm not too sure actually.
    // maybe when selecting a character and communicating with the network manager with player positions and stuff
    // keeps track of array of Leveling Systems and character stats
    // send updates to player controls
    // keeps track of xp and leveling ? Done
    // keeps track the players UI elements too ? Done
    public string myGirlName = "Player 105"; // this is the determining check if player manager connects to certain girl( player controls ) / this gets set when player selects their girl
    public int myGirlLevel = 0; // this is the level the girl has procressed and leveled up within the game
    public int myGirlPlayerNumber = 1; // the number the player is. 1-4, ETC: I'm player 2 so myGirlPlayerNumber = 2
    public ScriptableLevelingSystem[] levelSystem; // array of levels
    public PlayerControls myPlayer; // the player that is on this console/system. ignore all other players
    public string[] myPlayerMoves = new string[6];
    public Camera playerCam;
    public GameObject PressStart;

    #region Serializable Classes
    [System.Serializable]
    public class PlayerHealthTools
    {
        public Slider playerHealthSlider;
        public Text playerCurrentHealth;
        public Text playerMaxHealth;
    }    
    [Tooltip( "Players Health slider and values for the game to update" )]
    public PlayerHealthTools healthTools;

    [System.Serializable]
    public class PlayerXPTools
    {
        public Slider playerXPSlider;
        public Text playerCurrentXP;
        public Text playerMaxXP;
    }
    [Tooltip( "Players XP slider and values for the game to update" )]
    public PlayerXPTools xpTools;

    [System.Serializable]
    public class LevelUpTools
    {
        public GameObject parent;
        public Text newHealth;
        public Text newXP;
        public Text coinsGained;
        // add a section for getting a crystal and/or text saying crystals replenished
    }
    [Tooltip( "Level Up panel that appears when the player levels up" )]
    public LevelUpTools levelUpTools;

    [System.Serializable]
    public class DamageTakenTools
    {
        public GameObject parent;
        public Text value;
    }
    [Tooltip( "Damage Taken panel that appears when the player well...takes damage from enemys" )]
    public DamageTakenTools damageTakenTools;
    
    [System.Serializable]
    public class XPGainedTools
    {
        public GameObject parent;
        public Text value;
    }
    [Tooltip( "Experience Points panel that appears when the player well...gains XP from beating enemys and what not" )]
    public XPGainedTools xpGainedTools;
    
    [System.Serializable]
    public class UpdatePlayerHealthBarTool
    {
        public int sliderValue;
        public int difference;
        public int healthCounter;
        public int tempHealth = 0;
        public int sliderValueHolder;
        [HideInInspector] public bool isDying = false;
        [HideInInspector] public bool isLosingHealth = false;
    }
    [Tooltip( "Shows the progression of the XP slider when the player received xp points and levels up" )]
    public UpdatePlayerHealthBarTool healthBar;
    
    [System.Serializable]
    public class UpdateXPBarTool
    {
        public int sliderValue;
        public int difference;
        public int xpCounter;
        public int tempXP = 0;
        public int sliderValueHolder;
        [HideInInspector] public bool isLevelingUp = false;
        [HideInInspector] public bool isGettingXP = false;
    }
    [Tooltip( "Shows the progression of the XP slider when the player received xp points and levels up" )]
    public UpdateXPBarTool xpBar;
    #endregion

    #region Unity Functions
    public void Init( )
    {
        myPlayer.gameObject.SetActive( true );
        myPlayer.myName = myGirlName;
        //myPlayer.getInAwake.InputControls = new string[myPlayerMoves.Length];
        for( int i=0; i < myPlayer.getInAwake.InputControls.Length; i++ )
        {
            myPlayer.getInAwake.InputControls[i] = myPlayerMoves[i];
        }
        // get girls saved level
        healthTools.playerHealthSlider.value = healthTools.playerHealthSlider.maxValue = levelSystem[myGirlLevel].maxHealth;
        healthTools.playerCurrentHealth.text = healthTools.playerMaxHealth.text = levelSystem[myGirlLevel].maxHealth.ToString( );

        xpTools.playerXPSlider.value = 0; // save and get this number too...along with the girls level
        xpTools.playerCurrentXP.text = "0";
        xpTools.playerXPSlider.maxValue = levelSystem[myGirlLevel].maxXP;
        xpTools.playerMaxXP.text = levelSystem[myGirlLevel].maxXP.ToString( );
        levelUpTools.parent.SetActive( false );
        damageTakenTools.parent.SetActive( false );
        xpGainedTools.parent.SetActive( false );
        PressStart.SetActive( false );
    }
    void Update( )
    {
        /*if( Input.GetKeyDown( KeyCode.E ) ) //XP
        {
            UpdatePlayerXP( Random.Range( 100, 300 ) );
        }
        if( Input.GetKeyDown( KeyCode.Q ) ) // Health
        {
            UpdatePlayerHealth( Random.Range( 25, 125 ) );
        }*/
    }
    #endregion

    #region Player Health
    // enemys calls the playermanager singleton to deal damage and updates the UI
    // checks attack by sending parameters
    // make funtions that update player health through slider and texts
    public void UpdatePlayerHealth( int damage )
    {
        int value = (int)healthTools.playerHealthSlider.value - damage;
        healthBar.tempHealth += damage;
        healthBar.difference += damage;
        if( !healthBar.isLosingHealth ) StartCoroutine( HEALTH( ) );

        damageTakenTools.value.text = damage.ToString( );
        damageTakenTools.parent.SetActive( false );
        StartCoroutine( TakenDamage( ) );
    }
    IEnumerator HEALTH( )
    {
        healthBar.isLosingHealth = true;
        healthBar.sliderValue = (int)healthTools.playerHealthSlider.value;
        healthBar.sliderValueHolder = levelSystem[myGirlLevel].maxHealth;
        //healthBar.difference = Mathf.Abs( levelSystem[myGirlLevel].maxHealth - ( healthBar.sliderValue + healthBar.tempHealth ) );
        healthBar.healthCounter = 0;
        while( healthBar.healthCounter <= healthBar.tempHealth && !healthBar.isDying )
        {
            healthTools.playerHealthSlider.value = healthBar.sliderValue;
            healthTools.playerCurrentHealth.text = healthBar.sliderValue.ToString( );
            healthBar.sliderValue--;
            healthBar.healthCounter++;
            yield return new WaitForSeconds( 0.01f );
            if( healthTools.playerHealthSlider.value <= 0 )
            {
                // we died so do something here
                healthBar.isDying = true;
                healthTools.playerHealthSlider.value = 0;
                healthTools.playerCurrentHealth.text = "DED";

                yield return new WaitForSeconds( 1.5f ); // wait for leveled up animation and prompt to pop up then close it
            }
        }
        healthBar.sliderValue--;
        healthBar.tempHealth = 0;
        healthBar.healthCounter = 0;
        healthBar.sliderValue = 0;
        healthBar.sliderValueHolder = 0;
        healthBar.isLosingHealth = false;
    }
    IEnumerator TakenDamage( )
    {
        yield return new WaitForSeconds( 0.25f );
        if( healthBar.isLosingHealth && !xpBar.isLevelingUp )
        {
            damageTakenTools.parent.SetActive( true );
            yield return new WaitForSeconds( 3f );
        }
        damageTakenTools.parent.SetActive( false );
        healthBar.difference = 0;
    }
    #endregion

    #region XP Bar
    // function that updates the xp bar and levels up the girl when they beat an enemy/boss and complete levels
    public void UpdatePlayerXP( int xp )
    {
        xpBar.tempXP += xp;
        xpBar.difference -= xp;
        if( !xpBar.isGettingXP ) StartCoroutine( XP( ) );
        
        xpGainedTools.value.text = xp.ToString( );
        xpGainedTools.parent.SetActive( false );
        StartCoroutine( GainXP( ) );
    }
    IEnumerator XP( )
    {
        xpBar.isGettingXP = true;
        xpBar.sliderValue = (int)xpTools.playerXPSlider.value;
        xpBar.sliderValueHolder = levelSystem[myGirlLevel].maxXP;
        xpBar.difference = Mathf.Abs( levelSystem[myGirlLevel].maxXP - ( xpBar.sliderValue + xpBar.tempXP ) );
        xpBar.xpCounter = 0;
        while( xpBar.xpCounter <= xpBar.tempXP && !xpBar.isLevelingUp )
        {
            xpTools.playerXPSlider.value = xpBar.sliderValue;
            xpTools.playerCurrentXP.text = xpBar.sliderValue.ToString( );
            xpBar.sliderValue++;
            xpBar.xpCounter++;
            yield return new WaitForSeconds( 0.01f );
            if( xpTools.playerXPSlider.value >= levelSystem[myGirlLevel].maxXP )
            {
                xpBar.isLevelingUp = true;
                myGirlLevel++; // save the girls level
                // set active level up panel that shows
                healthTools.playerHealthSlider.value = healthTools.playerHealthSlider.maxValue = levelSystem[myGirlLevel].maxHealth;
                healthTools.playerCurrentHealth.text = healthTools.playerMaxHealth.text = levelSystem[myGirlLevel].maxHealth.ToString( );

                xpTools.playerCurrentXP.text = "0";
                xpTools.playerXPSlider.value = 0;
                xpTools.playerXPSlider.maxValue = levelSystem[myGirlLevel].maxXP;
                xpTools.playerMaxXP.text = levelSystem[myGirlLevel].maxXP.ToString( );
                
                levelUpTools.parent.SetActive( true );
                levelUpTools.newHealth.text = levelSystem[myGirlLevel].maxHealth.ToString( );
                levelUpTools.newXP.text = levelSystem[myGirlLevel].maxXP.ToString( );
                levelUpTools.coinsGained.text = levelSystem[myGirlLevel].coinsGained.ToString( );                
                
                xpBar.tempXP = Mathf.Abs( xpBar.difference );
                xpBar.difference = Mathf.Abs( xpBar.sliderValueHolder - xpBar.tempXP );
                xpBar.sliderValueHolder = levelSystem[myGirlLevel].maxXP;
                xpBar.xpCounter = 0;
                xpBar.sliderValue = 0;

                yield return new WaitForSeconds( 1.5f ); // wait for leveled up animation and prompt to pop up then close it
                levelUpTools.parent.SetActive( false );
                xpBar.isLevelingUp = false;
            }
        }
        xpBar.sliderValue--;
        xpTools.playerXPSlider.value = xpBar.sliderValue;
        xpTools.playerCurrentXP.text = xpBar.sliderValue.ToString( );
        xpBar.tempXP = 0;
        xpBar.xpCounter = 0;
        xpBar.sliderValue = 0;
        xpBar.sliderValueHolder = 0;
        xpBar.isLevelingUp = false;
        xpBar.isGettingXP = false;
    }
    IEnumerator GainXP( )
    {
        yield return new WaitForSeconds( 0.25f );
        if( !xpBar.isLevelingUp )
        {            
            xpGainedTools.parent.SetActive( true );
            yield return new WaitForSeconds( 3f );
        }
        xpGainedTools.parent.SetActive( false );
    }
    #endregion
}
