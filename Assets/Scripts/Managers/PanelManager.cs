﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelManager : MonoBehaviour
{
    /// keeps track of transitions/ladders
    /// keeps an EnemyManagerPool
    /// Number of Enemys needed to defeat before the ladders appear
    /// TextManager to display the status of the panel

    [System.Serializable]
    public class Variables
    {

    }
}
