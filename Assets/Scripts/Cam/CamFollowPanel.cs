﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollowPanel : MonoBehaviour
{
    public bool isRailingOnPanel = false;
    public GameObject player; // get this from the cam trigger when we enter
    public Transform camPlace; // get this from the cam trigger when we enter

    void Update( )
    {
        if( isRailingOnPanel )
        {
            TrackPlayer( );
        }
    }

    public void SetPosition( )
    {
        transform.position = camPlace.position;
    }

    void TrackPlayer( )
    {
        transform.position = new Vector3( player.transform.position.x, camPlace.position.y, camPlace.position.z );
        //transform.localEulerAngles = new Vector3( camPlace.localEulerAngles.x * -1, 0, 0 );
    }
}
