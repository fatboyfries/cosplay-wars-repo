﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackHitBox : MonoBehaviour
{
    public PlayerControls pMan;
    public int damage;
    public bool puchBack;

    void OnTriggerEnter( Collider other )
    {
        if( other.tag == "Enemy" )
        {
            // deal damage here
            damage = pMan.ReturnAttackDamage( );
            // call enemy take damage function here

            // check if attack has push and handle that in here
            puchBack = pMan.ReturnAttackPushBack( );
            if( puchBack )
            {
                // call enemy push back
            }
        }
    }
}
